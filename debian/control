Source: python-asgi-redis
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 python3-all,
 python3-asgiref,
 python3-msgpack,
 python3-redis,
 python3-setuptools,
 python3-six,
Standards-Version: 4.1.1
Homepage: https://github.com/django/asgi_redis/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-asgi-redis
Vcs-Git: https://salsa.debian.org/python-team/packages/python-asgi-redis.git
Testsuite: autopkgtest-pkg-python

Package: python3-asgi-redis
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: Redis-py backend for ASGI, with built-in sharding (Python3 version)
 An ASGI channel layer that uses Redis as its backing store, and supports both a
 single-server and sharded configurations, as well as group support.
 .
 A "local and remote" mode is also supported, where the Redis channel layer
 works in conjunction with a machine-local channel layer (asgi_ipc) in order to
 route all normal channels over the local layer, while routing all single-reader
 and process-specific channels over the Redis layer.
 .
 "Sentinel" mode is also supported, where the Redis channel layer will connect
 to a redis sentinel cluster to find the present Redis master before writing or
 reading data.
 .
 This package contains the Python 3 version of the library.
